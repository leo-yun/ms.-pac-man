﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pinkGhost : MonoBehaviour
{
    public float speed = 0.6f;
    // public Vector3 greenGhostDir;
    // List<Vector3> validDir;
    //public Vector3[] validDir;

    public Vector3 up = Vector3.up;
    public Vector3 down = Vector3.down;
    public Vector3 left = Vector3.left;
    public Vector3 right = Vector3.right;

    Vector3 dir = Vector3.right;
    Vector3 dis = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        dir = genDir();
    }

    // Update is called once per frame
    void Update()
    {

        if (!valid(dir))
        {
            dir = genDir();
        }
        else
        { 
            dis = transform.position + dir;

            Vector3 p = Vector3.MoveTowards(transform.position, dis, speed);
            GetComponent<Rigidbody>().MovePosition(p);
        }



    }

    Vector3 genDir()
    {

        

        if (dir == up)
        {
            if (valid(right)) {
                return right;
            }
           // else if (valid(down))
            //{
            //    return down;
           // }
            else
            {
                return left;
            }

        }

        if (dir == right)
        {
            if (valid(down))
            {
                return down;
            }
            else
            {
                return up;
            }
        }

        if (dir == down)
        {
            if (valid(left))
            {
                return left;
            }
            else
            {
                return right;
            }
        }

        if (dir == left)
        {
            if (valid(down))
            {
                return down;
            }
            else
            {
                return up;
            }
        }
        else return Vector3.right;

    }



    bool valid(Vector3 dir)
    {
        Vector3 pos = transform.position;
        RaycastHit hit;

        Debug.DrawLine(pos, transform.position + dir * 4, Color.blue);
        if (Physics.Linecast(pos, transform.position + dir * 4, out hit))
            if (hit.collider.tag == "wall" && hit.distance <= 4)
            {
                Debug.DrawLine(pos, transform.position + dir * 4, Color.red);
                dir = Vector3.zero;
                return (false);

            }
        return (true);

    }

    bool wallValid(Vector3 dir)
    {
        Vector3 pos = transform.position;
        RaycastHit hit;

        Debug.DrawLine(pos, transform.position + dir * 4, Color.blue);
        if (Physics.Linecast(pos, transform.position + dir * 4, out hit))
            if (hit.collider.tag == "wall" && hit.distance <= 4)
            {
                Debug.DrawLine(pos, transform.position + dir * 4, Color.red);
                dir = Vector3.zero;
                return (false);

            }
        return (true);

    }

}
