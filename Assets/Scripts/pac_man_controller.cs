﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pac_man_controller : MonoBehaviour
{

    //public GameObject dots;
    //dotGenerator Generator;

    public int point;


    public float speed = 0.4f;

    public Vector3 dir = Vector3.zero;
    public Vector3 dis = Vector3.zero;
    // Start is called before the first frame update

   
    void Start()
    {


        //dir = transform.position;
        dis = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()  
    {
      
        Vector3 p = Vector3.MoveTowards(transform.position, dis, speed);
        GetComponent<Rigidbody>().MovePosition(p);
     


        if (Input.GetKey(KeyCode.UpArrow) && valid(Vector3.up))
        {
            dir = Vector3.up;
           
        }
        if (Input.GetKey(KeyCode.RightArrow) && valid(Vector3.right))
        {
            dir = Vector3.right;
            
        }
        if (Input.GetKey(KeyCode.DownArrow) && valid(Vector3.down))
        {
            dir = Vector3.down;
            
        }
        if (Input.GetKey(KeyCode.LeftArrow) && valid(Vector3.left))
        {
            dir = Vector3.left;
         
        }


        if (valid(dir))
        {
            // Debug.Log("valid");
            dis = transform.position + dir;
            //dir = Vector3.zero;
        }

       // Vector3 temp = dis - (Vector3)transform.position;
        GetComponent<Animator>().SetFloat("dirX", dir.x);
        GetComponent<Animator>().SetFloat("dirY", dir.y);




    }

    bool valid(Vector3 dir)
    {
        //// Cast Line from 'next to Pac-Man' to 'Pac-Man'
        //Vector3 pos = transform.position;
        //Ray ray = new Ray(pos, dir);
        //Debug.DrawRay(pos,dir,Color.blue);
        //RaycastHit hit;

        //if (Physics.Linecast(ray, out hit, 4))
        //{
        //    // 如果射线与平面碰撞，打印碰撞物体信息  
        //   // Debug.Log("碰撞对象: " + hit.collider.name);
        //    // 在场景视图中绘制射线  
        //    Debug.DrawLine(ray.origin, hit.point, Color.red);
        //    if (hit.collider.tag == "wall")
        //        return (false);
        Vector3 pos = transform.position;
        //Ray ray = new Ray(pos, transform.position + dir);
        RaycastHit hit;

        Debug.DrawLine(pos, transform.position + dir * 4, Color.blue);
        if (Physics.Linecast(pos, transform.position + dir * 4, out hit))
            if (hit.collider.tag == "wall" && hit.distance <= 4)
            {
                Debug.DrawLine(pos, transform.position + dir * 4, Color.red);
                return (false);
            }
        return (true);


        // return (true);

        // RaycastHit2D hit = Physics2D.Linecast(pos + dir, pos);

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "powerpill(Clone)")
        {
             point += 10;
            other.gameObject.SetActive(false);

        }
        if (other.gameObject.name == "powerpowerpill(Clone)")
        {
            point += 1000;
            Debug.Log("powerpowerpill");
           
            other.gameObject.SetActive(false);


        }

        if (other.gameObject.name == "cherry(Clone)")
        {
            point += 100 ;
            Debug.Log("cherry");
            GameObject.Find("dots").GetComponent<dotGenerator>().hasFruit = false;
            GameObject.Find("dots").GetComponent<dotGenerator>().Level  += 1;
            other.gameObject.SetActive(false);


        }
        if (other.gameObject.name == "strawberry(Clone)")
        {
            point += 200;
            Debug.Log("strawberry");
            GameObject.Find("dots").GetComponent<dotGenerator>().hasFruit = false;
            GameObject.Find("dots").GetComponent<dotGenerator>().Level  += 1;
            other.gameObject.SetActive(false);



        }
        if (other.gameObject.name == "orange(Clone)")
        {
            point += 500;
            Debug.Log("orange");
            GameObject.Find("dots").GetComponent<dotGenerator>().hasFruit = false;
            GameObject.Find("dots").GetComponent<dotGenerator>().Level  += 1;
            other.gameObject.SetActive(false);


        }
        if (other.gameObject.name == "pretzel(Clone)")
        {
            point += 700;

            Debug.Log("pretzel");
            GameObject.Find("dots").GetComponent<dotGenerator>().hasFruit = false;
            GameObject.Find("dots").GetComponent<dotGenerator>().Level  += 1;
            other.gameObject.SetActive(false);


        }
        if (other.gameObject.name == "pear(Clone)")
        {
            point += 1000;

            Debug.Log("pear");
            GameObject.Find("dots").GetComponent<dotGenerator>().hasFruit = false;
            GameObject.Find("dots").GetComponent<dotGenerator>().Level  += 1;
            other.gameObject.SetActive(false);


        }
        if (other.gameObject.name == "banana(Clone)")
            {
                point += 2000;
            Debug.Log("banana");
            GameObject.Find("dots").GetComponent<dotGenerator>().hasFruit = false;
            GameObject.Find("dots").GetComponent<dotGenerator>().Level +=1;
            other.gameObject.SetActive(false);




        }



        if (other.gameObject.tag == "wall")
            dir = Vector3.zero;
        //other.gameObject.SetActive(false);





    }
    }



