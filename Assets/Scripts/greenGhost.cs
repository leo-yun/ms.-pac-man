﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class greenGhost : MonoBehaviour
{
    public float speed = 0.6f;
    // public Vector3 greenGhostDir;
    // List<Vector3> validDir;
    //public Vector3[] validDir;

    public Vector3 up = Vector3.up;
    public Vector3 down = Vector3.down;
    public Vector3 left = Vector3.left;
    public Vector3 right = Vector3.right;

    Vector3 dir = Vector3.zero;
    Vector3 dis = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        dir = genDir();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (valid(dir))
        {
            dis = transform.position + dir;
            Vector3 p = Vector3.MoveTowards(transform.position, dis, speed);
            GetComponent<Rigidbody>().MovePosition(p);
        }
        else { dir = genDir(); }



    }

    Vector3 genDir()
    {
        List<Vector3> temp = validDirArr();
        if (temp.Count < 1)
        {
            return (Vector3.zero);
        }
        Debug.Log("genDir" + temp[Random.Range(0, (temp.Count - 1))]);

        return (temp[Random.Range(0, (temp.Count))]);


    }

    List<Vector3> validDirArr()
    {
        List<Vector3> tempArr = new List<Vector3>();

        Vector3 pos = transform.position;
        if (valid(up)) { tempArr.Add(up); }
        if (valid(down)) { tempArr.Add(down); }
        if (valid(left)) { tempArr.Add(left); }
        if (valid(right)) { tempArr.Add(right); }


        //  Debug.Log("temp"+tempArr.);
        return (tempArr);
    }

    bool valid(Vector3 dir)
    {
        Vector3 pos = transform.position;
        RaycastHit hit;

        Debug.DrawLine(pos, transform.position + dir * 4, Color.blue);
        if (Physics.Linecast(pos, transform.position + dir * 4, out hit))
            if (hit.collider.tag == "wall" && hit.distance <= 4)
            {
                Debug.DrawLine(pos, transform.position + dir * 4, Color.red);
                return (false);
            }
        return (true);
    }


}


