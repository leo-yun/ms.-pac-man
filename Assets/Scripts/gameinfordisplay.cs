﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameinfordisplay : MonoBehaviour
{

    //public GameObject point;
    //public GameObject level;
    public Text point;
    public Text level;
    // Start is called before the first frame update
    void Start()
    {

        point = GameObject.Find("pointNumber").GetComponent<Text>();
        point.text = GameObject.Find("pac-man").GetComponent<pac_man_controller>().point.ToString();
        level = GameObject.Find("levelNumber").GetComponent<Text>();
        level.text = GameObject.Find("dots").GetComponent<dotGenerator>().Level.ToString();
    }


    // Update is called once per frame
    void Update()
    {
        point.text = GameObject.Find("pac-man").GetComponent<pac_man_controller>().point.ToString();
        level.text = GameObject.Find("dots").GetComponent<dotGenerator>().Level.ToString();
    }
}
