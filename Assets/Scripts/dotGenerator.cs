﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dotGenerator : MonoBehaviour
{
    public GameObject dots;

    
    public GameObject cherry;
    //100     
    public GameObject Strawberry;
    //200
    public GameObject orange;
    //500     
    public GameObject pretzel;
    //700
    public GameObject pear;
    //1000    
    //public GameObject apple;
    //2000
    public GameObject banana;
    //5000   


    public GameObject powerpowerpill;



 

    public  int Level = 1;
    public bool hasFruit = false;
    public  float times = 2;


   

    void Start()
    {

        Level = 1;
        times = 2;
        hasFruit = false;

        foreach (Transform child in transform)
        {
            if (child.tag == "dot")
            {
                Instantiate(dots, new Vector3(child.transform.position.x, child.transform.position.y, child.transform.position.z), Quaternion.identity,child);

            }
            
        }
          genPowerPill();
         


    }

    void Update()
    {


        //Debug.Log(hasFruit);

        //Debug.Log("Level:"+Level);
        if (hasFruit == false)
        {
            times -= Time.deltaTime;
        }

        else
       {
         times = Level * 2;
         }

        if (times  <= 0 && hasFruit == false)
        {
            int ramdNum = Random.Range(0, transform.childCount);
            if (transform.GetChild(ramdNum).childCount <2)
            {
                Debug.Log("draw");
                genBonus(ramdNum);
                hasFruit = true;
               
            }
        }
    }

   void genBonus(int index)
    {
        transform.GetChild(index).GetChild(0).gameObject.SetActive(false);

         GameObject temp = cherry;

        

        float FruitProb = Random.Range(0, 99);

        switch (FruitProb)
        {
            case float n when (n < 95 && n >= 92):
                temp = banana;
                break;

            //case float n when (n < 95 && n >= 92):
               // temp = apple;
               // break;

            case float n when (n < 92 && n >= 87):
                temp = pear;
                break;

            case float n when (n < 87 && n >= 80):
                temp = pretzel;
                break;

            case float n when (n < 80 && n >= 70):
                temp = orange;
                break;

            case float n when (n < 70 && n >= 50):
                temp = Strawberry;
                break;

            case float n when (n < 50):
                temp = cherry;
                break;
        }


        Instantiate(temp, new Vector3(transform.GetChild(index).transform.position.x, transform.GetChild(index).transform.position.y, transform.GetChild(index).transform.position.z), Quaternion.identity, transform.GetChild(index));
        
    }


    void genPowerPill() {
        int ramdNum = Random.Range(0, transform.childCount);
        Debug.Log(ramdNum);

        //transform.GetChild(ramdNum).gameObject.SetActive(false);

        Instantiate(powerpowerpill, new Vector3(transform.GetChild(ramdNum).transform.position.x, transform.GetChild(ramdNum).transform.position.y, transform.GetChild(ramdNum).transform.position.z), Quaternion.identity, transform.GetChild(ramdNum));

        transform.GetChild(ramdNum).GetChild(0).gameObject.SetActive(false);
        
    }
    
   

}
