
# Assessment 3 Preparing your Assets and Planning Your Project


* * *

# 1.Visual assets

`The 3D models have been created by me in the following list, however, they will be shown in the 2D levelbackground. 3D model could provide more details during the animation of assets to make the game more immersive and attractive, meanwhile, it also can reduce the duplicated work of 2D modelling.`

## Pacman

![pacman](images/pacman.png)

The pacman is the main character which will be controlled by the player.

**Animation:** move into four direction, open and close the mouth.

## Ghosts

![ghosts](images/ghosts.png)

There are four ghosts as enemy in the game with the different animation as following:

Green – Move randomly at each junction in the level

Pink  – Move clock-wise around the level

Blue – Run away from Ms. Pac-man

Red  – Chase Ms. Pac-man

The mouths and eyes of the ghosts can open and close. If the pacman collides with the ghosts, the game will be over.

![dark](images/dark.png)

Once the pacman collides with a powerpowerpill, all the ghost will become dark blue and looks like the image show above.

## Fruits and powerpills

![fruit](images/fruit.png)

The fruits will appear randomly during the game and replace the location of powerpills. Once the pacman eat fruits, the play can get higher marks. There are many kinds of fruits, including Banana, Cherry, Orange, Pear, Strawberry and Pretzel. The marks of the various fruits are different.

## Level boundary

![Level boundary](images/level-b.png)

This is the level boundary in the game and all characters will move within the boundary.

## Level background

In order to avoid the visual interference by colourful background, thus, I would like to use the black background – dafult setting. This will enhance the visual contrast so that other colourful objects and characters will be evident and bright.

## Level boundary

## Points and Level

![Points and Level](images/point.png)

The information of points the player get and the level of game will be shown in the top of the game screen.

## Effect sprite animations

![Effect sprite animations](images/eff.png)

All animations of sprite have been located in the animations file in order to enhance the organisation of the project.

* * *

# 2.Audio Assets

![audio](images/audio.png)

`The screenshot shows the audio assets imported in the project, while the audio files list shows the audio assets which would be used in the game.`

## eatfruit

The sound effect will be activated when player collide with any kind of fruit in the game.  This crispy sound stimulates the real sound of eating fruit, which may help to establish an immersive game experience.

Download from: <https://freesound.org/people/thedapperdan/sounds/199923/>

## gameBGM

The audio will be played during playing the whole game, and the arcade style background music may excite the player. This background music should play loop.

Download from: <https://opengameart.org/content/pacman-clone-background-music>

## gameBGM-reverser

The audio will be played after the player collide with a powerpowerbill and the game background will be changed to provide audio feedback to the player that invincible mode has been activated.

Download from: <https://opengameart.org/content/pacman-clone-background-music>

## menuBGM

The audio is the background music of menu screen.

Download from: <https://freesound.org/people/joshuaempyre/sounds/251461/>

## menuSelect

The audio will be played when the option in the menu screen has been selected.

Download from:  <https://freesound.org/people/TheDweebMan/sounds/277216/>

## pacmanDeath

This sound effect will be played when the pacman is dead.

Download from: <https://freesound.org/people/VincentM400/sounds/249618/>

## pacmanHitWall

When the pacman hit the wall, the sound feedback will be provided to notify the player that he cannot move forward.

Download from: <https://freesound.org/people/VincentM400/sounds/249618/>

## powerbill

At the moment that the pacman eats a powerbill, the sound effect will be played.

Download from: <https://freesound.org/people/ProjectsU012/sounds/341695/>

* * *

# 3.Recreated level scene

![scene](images/scene.png)

This is the screen of initial layout of the recreated level.

All characters and object will show followed by the specific rules. The pacman can move in four directions to eat powerpills, and some fruits with higher markers will appear randomly. The ghosts will also move and pacman should avoid colliding with the ghost. On the top of the screen, there is the information bar that shows the points a player gets and the current level.

In the further development, some visual effects will be added. For example, when the pacman eats powerpills and fruits, the marks will show as “+ 100”. Once they got higher marks, the colour of the mark shown on the screen will be highlighted. Meanwhile, some decorations of the level will be created to make the game screen looks better and immersive.

* * *

# 4.Git Repository

## Git commit history
![git](images/git.png)

## Brach Structure
![branches](images/branches.png)

There are there main branches, including Master, Feature and DEV. The Master branch save the information about the work history, and the Develop branch works as integration for the feature branches. For the feature, there are several branches, such as Basic-game, enemy, dot and menu.

**[Feature/Basic-game]** Visual asset design &. script: the basic animation of the pacman, 3D model of pacman, Maze design, level boundary and related sound effects.

**[Feature/Basic-game-enemy]** Visual asset design &. script: the basic animation of the ghost, 3D model of the ghost and related sound effects.

**[Feature/Basic-game-dot]** Visual asset design &. script: fruits and powerpills, information bar and related sound effects.

**[Feature/Menu]** The visual assets in menu and scenes design.
* * *

# 5.Plan for coded system

## Movement

Pacman: Move in four direction while the head of pacman should turn to the direction it will move.

Green Ghost: randomly move in the level

Pink Ghost: move clock-wise around the level

Blue Ghost: run away from pacman

Red Ghost: move to chase the pacman

Dark Ghost: run away from the pacman and move randomly

## User-input

The user can use a keyboard or a mouse to select the relevant option.

The user can change slider to adjust volume of sound effects and background music

The user can control the movement of the pacman

The user can pause and resume the game pressing ESC.

## Object behaviours

The information bar can reflect the points a player obtains and the current level, while the points may be updated as the player get more points.

The powerpill and fruits will randomly appear, and they contain different points.

## Game rules and collision

When the pacman eat powerpill and fruits, the player obtain the related points.

When the pacman collides with the ghost, the game will be over.

There is only one fruit show on the screen, and once the fruit has been eaten, a new fruit will appear.

The frequency of the appearance of fruits will be different according to the difficulties.

The powerpill will appear in the fixed location at the beginning of the game.

Two powerpowerpills will appear at the random location to replace the location of powerpill in the start of the game.

When the pacman collide with the powerpowerpill, the movement speed of pacman will increase, while all the ghosts will become dark ghost.

When the pacman collide the dark ghost, the ghost will be destroyed instead of game over.

If there is no powerpill or ghosts in the screen, the player will win and the game turns to the next difficulty.

## Playing of animation and audio

The background music of menu and game will playback in a loop.

Sound effects will be played once the pacman hit the wall, eat powerpills and fruits, win the game as well as dead.

## Interface

In the menu, the play can select the different difficulties of the game

In the menu, there will be two main options in the middle of the screen- ‘start game’ or ‘quit’

The play can adjust the volume of sound effect in the menu or in the pause screen.

In the pause screen, there will be two main option - ‘resume’ and ‘quit’

An information bar will be located at the top of the game screen, while the game level boundary will show in the middle of the screen.

* * *

# 6.Design Proposal

`Pacman is a maze arcade game that a player needs to control the pacman to eat powerpills and fruits to get higher marks and complete the higher level. This original game is very interesting and classic; however, some design will be improved in my project to establish an immersive game experience for the player.`
## Random maze generation

As this is a maze arcade game, it is necessary to design more mazes for the player. To increase the playability, the project plans add more maze and the maze will be generated randomly at the start of the game. The different maze contains different shape of level boundaries and background.

## Better visual effects

To establish a more immersive and attractive game experience, better visual effects would be designed, such as the pop-up animations. For example, when the pacman eats a fruit, a small clip of animation will show up in the screen. The clip of animation is that a big pacman open a big mouth to eat the fruit, which last for 2 seconds. In addition, some slight improvement will also be added to create a better visual effect.

## 3D character

3D character has been established compared to the original game to provide more details of the characters. Thus, the characters and objects will be more vivid. Meanwhile, it also can reduce the duplicated work of 2D modelling.

## Audio feedback

The audio in the game is quite similar with the original game, they are all the similar arcade style. However, all sound effects and background music has been changed. The background music will be changed when the change of the game mode and level.

## Game rule innovation – various difficulties

### The project will add more difficulties mode as innovation to provide more options to the players.

 **Easy**: normal frequency of the appearance of fruits, low speed of movement of ghost

**Moderate**: the appearance of fruits become more frequent, the ghost movement speed become more fast

**Hard**: The fastest speed of ghost movement, the appearance of fruits and powerpowerpills have a time limit.

### Safety circle

This is a new rule of the game. When the fruit appear, there is a safety circle base on the location of the fruit. The safety circle shrinks and last for a few seconds, then it will disappear. If the pacman is not in the safety circle, the marks will be deducted every second. If the mark will be deducted to a negative number, the game is over as well.


